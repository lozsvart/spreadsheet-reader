package com.gitlab.lozsvart.spreadsheetreader;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.gitlab.lozsvart.spreadsheetreader.RecordSpreadsheetProcessPolicy.ALLOW_REPEATING_HEADERS;
import static com.gitlab.lozsvart.spreadsheetreader.RecordSpreadsheetProcessPolicy.COMMA_SEPARATE_MULTIPLE_VALUES;

/**
 * Exposes methods to parse .xls and .xlsx spreadsheets.
 */
public class SpreadsheetReader {

    private static final DataFormatter DATA_FORMATTER = new DataFormatter();

    private static final Set<RecordSpreadsheetProcessPolicy> DEFAULT_POLICIES = new HashSet<>();

    /**
     * Reads a spreadsheet in record format.
     *
     * @param file            the file containing the workbook
     * @param spreadsheetName the name of the spreadsheet to parse
     * @param rowProcessor    an object that will be passed each row in a parsed format
     */
    public static void readRecordSpreadsheet(File file, String spreadsheetName, Consumer<ParsedRow> rowProcessor) {
        readRecordSpreadsheet(file, spreadsheetName, rowProcessor, DEFAULT_POLICIES);
    }

    /**
     * Reads a spreadsheet in record format.
     *
     * @param file            the file containing the workbook
     * @param spreadsheetName the name of the spreadsheet to parse
     * @param rowProcessor    an object that will be passed each row in a parsed format
     * @param policies        set of rules to use during parsing
     */
    public static void readRecordSpreadsheet(File file, String spreadsheetName, Consumer<ParsedRow> rowProcessor, Set<RecordSpreadsheetProcessPolicy> policies) {
        new SpreadsheetProcessing(file, spreadsheetName, rowProcessor, policies).execute();
    }

    private static class SpreadsheetProcessing {

        private final File file;
        private final String spreadsheetName;
        private final Consumer<ParsedRow> rowProcessor;
        private final Set<RecordSpreadsheetProcessPolicy> policies;

        private Map<Integer, String> headers;
        private Sheet sheet;
        private Iterator<Row> rowIterator;
        private FileInputStream inputStream;

        private SpreadsheetProcessing(File file, String spreadsheetName, Consumer<ParsedRow> rowProcessor, Set<RecordSpreadsheetProcessPolicy> policies) {
            this.file = file;
            this.spreadsheetName = spreadsheetName;
            this.rowProcessor = rowProcessor;
            this.policies = policies == null ? DEFAULT_POLICIES : new HashSet<>(policies);
        }

        public void execute() {
            extractSpreadsheet();
            extractHeaders();
            validateContent();
            processRows();
            cleanUp();
        }

        private void cleanUp() {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new SpreadsheetProcessingException("Unable to close file.");
                }
            }
        }

        private void processRows() {
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                rowProcessor.accept(parse(headers, row));
            }
        }

        private void extractHeaders() {
            rowIterator = sheet.rowIterator();
            Row headersRow = rowIterator.next();
            headers = new HashMap<>();
            for (Cell cell : headersRow) {
                headers.put(cell.getColumnIndex(), getValue(cell));
            }
        }

        private void extractSpreadsheet() {
            Workbook input;
            try {
                input = getWorkbook(file);
            } catch (IOException | InvalidFormatException e) {
                throw new SpreadsheetProcessingException("Can not read workbook file", e);
            }
            sheet = input.getSheet(spreadsheetName);
        }

        private ParsedRow parse(Map<Integer, String> headers, Row row) {
            Map<String, List<Cell>> cellsByHeader = new HashMap<>();
            for (int columnIndex = 0; columnIndex < row.getLastCellNum(); columnIndex++) {
                String header = headers.get(columnIndex);
                Cell cell = row.getCell(columnIndex);
                List<Cell> value = new LinkedList<>();
                value.add(cell);
                value.addAll(cellsByHeader.getOrDefault(header, Collections.emptyList()));
                cellsByHeader.put(header, value);
            }

            return new ParsedRow() {
                @Override
                public String getFormatted(String column) {
                    return formatCells(cellsByHeader.getOrDefault(column, Collections.emptyList()));
                }

                @Override
                public int getAsInt(String column) {
                    return getFirstByName(column, cellsByHeader)
                            .filter(cell -> cell.getCellType() == CellType.NUMERIC)
                            .map(cell -> (int) cell.getNumericCellValue())
                            .orElse(0);
                }
            };
        }

        private Optional<Cell> getFirstByName(String column, Map<String, List<Cell>> cellsByHeader) {
            return cellsByHeader.getOrDefault(column, Collections.emptyList()).stream()
                    .findFirst();
        }

        private void validateContent() {
            if (!policyExists(ALLOW_REPEATING_HEADERS)) {
                Set<String> uniqueHeaders = new HashSet<>(headers.values());
                if (uniqueHeaders.size() < headers.size()) {
                    throw new SpreadsheetProcessingException("Multiple headers exist with the same value");
                }
            }
        }

        private String formatCells(List<Cell> cells) {
            if (policyExists(COMMA_SEPARATE_MULTIPLE_VALUES)) {
                return cells.stream()
                        .map(SpreadsheetReader::getValue)
                        .collect(Collectors.joining(","));
            }
            return cells.stream()
                    .findFirst()
                    .map(SpreadsheetReader::getValue)
                    .orElse("");
        }

        private Workbook getWorkbook(File file) throws IOException, InvalidFormatException {
            Workbook input;
            String fileName = file.getName();
            inputStream = new FileInputStream(file);
            if (fileName.endsWith(".xlsx")) {
                input = new XSSFWorkbook(inputStream);
            } else if (fileName.endsWith(".xls")) {
                input = new HSSFWorkbook(inputStream);
            } else {
                throw new SpreadsheetProcessingException("Invalid file extension (.xlsx or .xls allowed)");
            }
            return input;
        }

        private boolean policyExists(RecordSpreadsheetProcessPolicy policy) {
            return policies.contains(policy);
        }

    }

    private static String getValue(Cell cell) {
        return DATA_FORMATTER.formatCellValue(cell);
    }

}
