package com.gitlab.lozsvart.spreadsheetreader;

import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;

import static com.gitlab.lozsvart.spreadsheetreader.RecordSpreadsheetProcessPolicy.*;
import static com.gitlab.lozsvart.spreadsheetreader.SpreadsheetReader.readRecordSpreadsheet;

public class MainTest {

    public static void main(String[] args) {
        Set<RecordSpreadsheetProcessPolicy> policies = new HashSet<>();
        policies.add(ALLOW_REPEATING_HEADERS);
        policies.add(COMMA_SEPARATE_MULTIPLE_VALUES);
        readRecordSpreadsheet(getFileByName("sample.xlsx"), "data", parsedRow -> {
            Map<String, String> rowMap = toMapWithHeaders(parsedRow, Arrays.asList("Name", "Age"));
            System.out.println(rowMap);
        }, policies);
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.schedule(() -> {}, 2, TimeUnit.MINUTES);
    }

    private static Map<String, String> toMapWithHeaders(ParsedRow parsedRow, List<String> headers) {
        HashMap<String, String> result = new HashMap<>();
        for (String header: headers) {
            result.put(header, parsedRow.getFormatted(header));
        }
        return result;
    }

    private static File getFileByName(String name) {
        URL resource = ClassLoader.getSystemClassLoader().getResource(name);
        if (resource != null) {
            return new File(resource.getFile());
        }
        return null;
    }

}
