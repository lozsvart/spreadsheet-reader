package com.gitlab.lozsvart.spreadsheetreader;

/**
 * Base exception class for spreadsheet processing errors.
 */
public class SpreadsheetProcessingException extends RuntimeException {

    /**
     * Creates an instance.
     *
     * @param message the exception message
     */
    public SpreadsheetProcessingException(String message) {
        super(message);
    }

    /**
     * Creates an instance.
     *
     * @param message the exception message
     * @param cause   the cause
     */
    public SpreadsheetProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}
