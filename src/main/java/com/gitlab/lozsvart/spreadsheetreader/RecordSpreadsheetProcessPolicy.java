package com.gitlab.lozsvart.spreadsheetreader;

/**
 * Rules for reading spreadsheet in record format.
 */
public enum RecordSpreadsheetProcessPolicy {
    /**
     * Allows to have multiple columns with the same header to be parsed. By default, only the first is read.
     */
    ALLOW_REPEATING_HEADERS,
    /**
     * Will enumerate all the read values of the field in a comma separated list.
     */
    COMMA_SEPARATE_MULTIPLE_VALUES
}
