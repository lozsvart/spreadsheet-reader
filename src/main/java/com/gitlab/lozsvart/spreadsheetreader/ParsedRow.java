package com.gitlab.lozsvart.spreadsheetreader;

/**
 * An interface to read rows parsed during SpreadsheetReader.readRecordSpreadsheet
 */
public interface ParsedRow {

    /**
     * Returns the content of the cell in the provided column formatted as a String.
     *
     * @param column the column name
     * @return the formatted value
     */
    String getFormatted(String column);

    /**
     * Returns the content of the cell in the provided column as an integer.
     *
     * @param column the column name
     * @return the integer value
     */
    int getAsInt(String column);

}
